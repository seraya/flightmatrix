/**
 * Created by sta on 24.11.14.
 */
// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var async      = require('async');
var cors       = require('cors');
var app        = express();
app.use(cors());

// set our port
var port     = process.env.PORT || 8080;


// FUNCTIONS TO ACCESS OTHER APIs
// =============================================================================

// input:  string address
// output: {lat, lng} coordinates
function getLatLng(address, callback) {
    var result =
    {
        "lat": 0,
        "lng": 0
    }
    var request = require("request");
    var key = "AIzaSyDUdbwQBviAn7qBTm16HmsUDlXz7zf9ANQ";
    var url = "https://maps.googleapis.com/maps/api/geocode/json?key=" + key + "&address=" + address + "&sensor=false";
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {

            // fill result
            result.lat = body.results[0].geometry.location.lat;
            result.lng = body.results[0].geometry.location.lng;

            // return callback
            console.log("log: got response from google geocode api");
            return callback(null, result);
        }
    })
}

// input:  {lat, lng} coordinates
// output: {code, name, lat, lng} airport
function getNearestAirport(coordinates, callback) {
    var request = require("request");

    var airport =
    {
        "code": "",
        "name": "",
        "lat": 0,
        "lng": 0
    };
    var url = "https://airport.api.aero/airport/nearest/" + coordinates.lat + "/" + coordinates.lng + "?user_key=fe8beced81c1804bb735ccd9cda988a4";
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {

            // fill result
            airport.code = body.airports[0].code;
            airport.name = body.airports[0].name;
            airport.lat = body.airports[0].lat;
            airport.lng = body.airports[0].lng;


            // return callback
            console.log("log: got response from aero airport api");
            return callback(null, airport);
        }
    })
}

// input:  string origin, string destination
//         (can be an address or string from type "41.43206,-81.38992")
// output: {from, to, price, distance, duration, by: "car"}
function getLegCar(origin, destination, callback) {
    var request = require("request");
    var gasPrice = 15;
    var leg = {
        "from": origin,
        "to": destination,
        "price": 0,
        "distance": 0,
        "duration": 0,
        "by": "car"
    };
    var url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destination + "&mode=car";
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {

            for(var i=0; i < body.rows.length; i++) {
                var row = body.rows[i];
                for (var j=0; j<row.elements.length; j++){

                    // fill result
                    var element = row.elements[j];
                    leg.price = element.distance.value * gasPrice / 100000;
                    leg.distance = element.distance.value/1000;
                    leg.duration = element.duration.value/60;

                    // return callback
                    console.log("log: got response from google distance matrix api");
                    return callback(null, leg);

                }
            }
        }
    })
}

// input:  string origin, string destination
//         (can be an address or string from type "41.43206,-81.38992")
// output: {from, to, price, distance, duration, by: "plane"}
function getLegPlane(origin, destination, date, callback) {
    var request = require("request");

    var leg = {
        "from": origin.name + " (" + origin.code + ")",
        "to": destination.name + " (" + destination.code + ")",
        "price": null,
        "distance": null,
        "duration": null,
        "by": "plane"
    };

    var requestData = {
        request: {
            slice: [
                {
                    origin: origin.code,
                    destination: destination.code,
                    date: date
                }
            ],
            passengers: {
                adultCount: 1,
                infantInLapCount: 0,
                infantInSeatCount: 0,
                childCount: 0,
                seniorCount: 0
            },
            solutions: 1,
            refundable: false
        }
    };

    apiKey = "AIzaSyB2P0a9-o1yLHGCHVFMJN73pNavkRWKvGU"; //"AIzaSyDUdbwQBviAn7qBTm16HmsUDlXz7zf9ANQ";
    url = "https://www.googleapis.com/qpxExpress/v1/trips/search?key=" + apiKey;

    request({
        url: url,
        method: "POST",
        json: true,
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(requestData)

    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            for(var i=0; i < body.trips.tripOption.length; i++) {

                // fill result
                leg.price = body.trips.tripOption[i].saleTotal.replace(/[^0-9\.]+/g, '');

                for(var j= 0; j < body.trips.tripOption[i].slice[0].segment[0].leg.length; j++) {
                    leg.distance += body.trips.tripOption[i].slice[0].segment[0].leg[j].mileage * 1.609344;
                }
                leg.duration = body.trips.tripOption[i].slice[0].duration;

                // return callback
                console.log("log: got response from google qpx express api");
                return callback(null, leg);
            }
        }
        else {
            console.log("error: " + error);
            console.log("response.statusCode: " + response.statusCode);
            console.log("response.statusText: " + response.statusText);
        }
    })
}

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next();
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

// on routes that end with /carvsplane
app.route('/carvsplane/:from/:to/:date')
    .get(function(req, res) {

        // read parameters and store into variables
        var from = req.params.from;
        var to = req.params.to;
        var date = req.params.date;

        // declare global variables
        var fromLatLng = null;
        var toLatLng = null;
        var originAirport = null;
        var destinationAirport = null;

        // call functions in parallel to avoid inconsistent results
        async.parallel([
            function(callback) {
                getLatLng(from, callback)
            },
            function(callback) {
                getLatLng(to, callback)
            }
        ], function (err, results) {

            // save results to global variables
            fromLatLng = results[0];
            toLatLng = results[1];

            // call functions in parallel to avoid inconsistent results
            async.parallel([
                function(callback) {
                    getNearestAirport(fromLatLng, callback)
                },
                function(callback) {
                    getNearestAirport(toLatLng, callback)
                }
            ], function (err, results) {

                // save results to global variables
                originAirport = results[0];
                destinationAirport = results[1];

                // call functions in parallel to avoid inconsistent results
                async.parallel([
                    function(callback) {
                        getLegCar(from, originAirport.lat + "," + originAirport.lng, callback)
                    },
                    function(callback) {
                        getLegPlane(originAirport, destinationAirport, date, callback)
                    },
                    function(callback) {
                        getLegCar(destinationAirport.lat + "," + destinationAirport.lng, to, callback)
                    },
                    function(callback) {
                        getLegCar(from, to, callback)
                    }
                ], function (err, results) {

                    // save results to global variables
                    var planeLegs = [ results[0], results[1], results[2] ];
                    var carLegs = results[3];

                    // init response and fill known attributes
                    var response = {
                        "bestPrice": 0,
                        "shortestDistance": 0,
                        "leastDuration": 0,
                        "bestPriceVehicle": "",
                        "shortestDistanceVehicle": "",
                        "leastDurationVehicle": "",
                        "car": carLegs,
                        "plane":
                        {
                            "from": from,
                            "to": to,
                            "price": 0,
                            "distance": 0,
                            "duration": 0,
                            "by": "plane",
                            "legs": []
                        }
                    };

                    // add plane data to response
                    for (var i = 0; i<planeLegs.length; i++)
                    {
                        // add meta data
                        response.plane.price += parseFloat(planeLegs[i].price);
                        response.plane.distance += parseFloat(planeLegs[i].distance);
                        response.plane.duration += parseFloat(planeLegs[i].duration);

                        // add legs
                        response.plane.legs.push(planeLegs[i]);
                    }

                    // aggregate and add meta data
                    if (response.plane.price < response.car.price) {
                        response.bestPrice = response.plane.price;
                        response.bestPriceVehicle = "plane";
                    }
                    else {
                        response.bestPrice = response.car.price;
                        response.bestPriceVehicle = "car";
                    }
                    if (response.plane.distance < response.car.distance) {
                        response.shortestDistance = response.plane.distance;
                        response.shortestDistanceVehicle = "plane";
                    }
                    else {
                        response.shortestDistance = response.car.distance;
                        response.shortestDistanceVehicle = "car";
                    }
                    if (response.plane.leastDuration < response.car.leastDuration) {
                        response.leastDuration = response.plane.duration;
                        response.leastDurationVehicle = "plane";
                    }
                    else {
                        response.leastDuration = response.car.duration;
                        response.leastDurationVehicle = "car";
                    }

                    // log results
                    console.log("SUCCESS!");
                    console.log("RESPONSE:");
                    console.log(response);

                    // return response to caller
                    res.setHeader("Access-Control-Allow-Origin", "*");
                    res.send(JSON.stringify(response, null, 4));

                })

            });

        });

    });

// REGISTER OUR ROUTES -------------------------------
app.use('/carvsplane', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log("");
console.log("           ___");
console.log("        .-v'---`\\.");
console.log("       /:/__ : __\\'.");
console.log("      |:/<O_>|<O>|:|         ");
console.log("      |:|\\_..J...|:|           vehicle comparison API V1.0");
console.log("     _|:|\\ t----j|:|_            ");
console.log("    /:`.| \\ \\__/ |.':\\                 purpose:");
console.log("   |:   |  |---: |   :|              _    rates the vehicles car and plane for a route");
console.log("    \\:  |.-::.:|-:._:/           /|.' '.    after duration, price and distance");
console.log("    / .-'  :'x71':  `\\           ||_   |  ");
console.log("    \\/\\...---.   :---.\\          |   .`   2014,");
console.log("    /.'`/ .'  ':'  '. \\       .'\\ .'   Seraya Takahashi");
console.log("   | : |..:...:|:....:.|\\    .'   '       ");
console.log("   | : |\\ :   /|\\   : /'.\\_.'   .`");
console.log("   | : | \\---':| `---'\\'./||  .'");
console.log("   | : | |;:  :|   :|  \\/::|.'");
console.log("   }==={ |;:  :|   :|   `--'");
console.log("   }==={/`'._  \\    _\\");
console.log("   | : |`'-._`-:|.-'.-\\");
console.log("   | : |\"\":\"\"\"\"\"|\"\"\":\"\"|");
console.log("   | : |  :    :|    : |");
console.log("   | : |  :    :|    : |");
console.log("    \\: |..:  . /    :..|");
console.log("    /  <      :|-   :  /");
console.log("   |...|      :|-   : /");
console.log("    \\_/.      :|-   :|");
console.log("       |      :|-   :|");
console.log("       \\      :|-   :/");
console.log("        |      |-   |");
console.log("");
console.log('HTTP SUCCESSFULLY STARTED ON PORT ' + port);
console.log("");